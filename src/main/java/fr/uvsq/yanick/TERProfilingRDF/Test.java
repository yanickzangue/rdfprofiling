//package fr.uvsq.yanick.TERProfilingRDF;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.jena.rdf.model.Model;
//import org.apache.jena.rdf.model.ModelFactory;
//import org.apache.jena.rdf.model.Property;
//import org.apache.jena.rdf.model.RDFNode;
//import org.apache.jena.rdf.model.Resource;
//import org.apache.jena.rdf.model.Statement;
//import org.apache.jena.rdf.model.StmtIterator;
//import org.apache.jena.vocabulary.OWL;
//import org.apache.jena.vocabulary.VCARD;
//
//public class Test extends Object{
//
//		public static void main (String args[]) {
//			// quelques définitions
//			String personURI    = "http://somewhere/JohnSmith";
//			String givenName    = "John";
//			String familyName   = "Smith";
//			String fullName     = givenName + " " + familyName;
//	
//			// créer un modèle vide
//			Model model = ModelFactory.createDefaultModel();
//	
//			// créer la ressource
//			//   et ajouter des propriétés en cascade
//			Resource johnSmith
//			= model.createResource(personURI)
//			.addProperty(VCARD.FN, fullName)
//			.addProperty(VCARD.N,
//					model.createResource()
//					.addProperty(VCARD.Given, givenName)
//					.addProperty(VCARD.Given, familyName));
//			// list the statements in the graph
//			StmtIterator iter = model.listStatements();
//	
//			//Profiling type
//			int k=0;
//			int vertex=0;
//			int arc=0;
//			int literal=0;
//			//List of profiling
//			ArrayList<String> Lvertex = new ArrayList();
//			ArrayList<String> Larc = new ArrayList();
//			ArrayList<String> Lliteral = new ArrayList();
//	
//			// print out the predicate, subject and object of each statement
//			while (iter.hasNext()) {
//				Statement stmt      = iter.nextStatement(); // get next statement
//				Resource  subject   = stmt.getSubject();   // get the subject
//				Property  predicate = stmt.getPredicate(); // get the predicate
//				RDFNode   object    = stmt.getObject();    // get the object
//				
//				//Construct list of Vertex arc and literal
//				Lvertex.add(subject.toString());
//				Larc.add(predicate.toString());
//				Lliteral.add(object.toString());
//				 
//				System.out.print(" " + predicate.toString() + " ");
//				if (object instanceof Resource) {
//					vertex++;
//					System.out.print(object.toString());
//				} else {
//					literal++;
//					// object is a literal
//					System.out.print(" \"" + object.toString() + "\"");
//				}
//				arc++;
//				vertex++;
//				System.out.println(" .");
//				     		   
//			}
//			
//			System.out.println("\n Profiling: \n " + model.getResource(personURI) +" : "+ " NVertex: "+vertex + " NArc: "+arc + " NLiteral: "+literal);
//		
//			for (String item : Lliteral) {
//				 System.out.println(item);
//			}
//			
//			   Map<String, Integer> seussCount = new HashMap<String,Integer>();
//			   for(String t: Lliteral) {
//			       Integer i = seussCount.get(t);
//			       if (i ==  null) {
//			           i = 0;
//			       }
//			       seussCount.put(t, i + 1);	
//			      
//			    }
//			   System.out.println("\n " +seussCount.toString());
//				
//			   
//			 
//			   
//			   Map<String, Integer> seuss = new HashMap<String,Integer>();
//			   for(String t: Larc) {
//			       Integer i = seuss.get(t);
//			       if (i ==  null) {
//			           i = 0;
//			       }
//			       seuss.put(t, i + 1);	
//			      
//			    }
//			   System.out.println("\n " +seuss.toString() );
//				 
//		}
//
//}
