package fr.uvsq.yanick.TERProfilingRDF;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VCARD;

public class LoadRdf {


	public static void main(String[] args) {
		/*
		 * dbpedia_2015-10.owl ;rdfexample.rdf; 20140114.rdf;
		 */

		String inputFileName = "dbpedia_2015-10.owl";
		// create an empty model
		Model model = ModelFactory.createDefaultModel();		
		// use the FileManager to find the input file
		InputStream in = FileManager.get().open(inputFileName);
		if (in == null) {
			throw new IllegalArgumentException( "File: " + inputFileName + " not found");
		}
		// read the RDF/XML file
		model.read( in, "" ); // ou  Model model = FileManager.get().loadModel(inputFileName);

					// recuperer les vocabilaire qui vienne du dataset
					ArrayList<String> lvoc = new ArrayList<>();
					ArrayList<String> lvocAbrege = new ArrayList<>();
					int nbrePropOfVocab=0;
					try {
						FileReader fr = new FileReader(inputFileName);
						BufferedReader br = new BufferedReader(fr);
						String str;
						while ((str = br.readLine()) != null) {
							String bar = StringUtils.substringBetween(str, "xmlns:", "=");
							String barNS = StringUtils.substringBetween(str, "=\"", "\"");
							if (bar != null) {
								lvoc.add(barNS);
								lvocAbrege.add(bar);

							}
						}
						br.close();

					} catch (IOException e) {
						System.out.println("File not found");
					}
					long st = lvoc.stream().distinct().count();//le nombre de vocabulaire du dataset
					
		StmtIterator iter = model.listStatements(); 

		ArrayList<Resource>lr=new ArrayList<Resource>();
		ArrayList<Property>lp=new ArrayList<Property>();
		ArrayList<RDFNode>lo=new ArrayList<RDFNode>();
		ArrayList<Property>lprwithlitteral=new ArrayList<Property>();
		ArrayList<RDFNode>loj=new ArrayList<RDFNode>();
		ArrayList<String> lrtype=new ArrayList<String>();//resources typées dupliquées
		ArrayList<String> lptype=new ArrayList<String>();//type predicat

		int i=0,j=0,t=0,nr=0,literal=0;
		long dimModel=model.size()+1;
		while (iter.hasNext()) {
			Statement stmt      = iter.nextStatement();  
			Resource  r   = stmt.getSubject();
			Property  predicate = stmt.getPredicate(); 
			RDFNode   obj   = stmt.getObject(); 
			if(predicate.isProperty()){
				++i;
				lp.add(predicate);
				if(obj.isLiteral()){

					lprwithlitteral.add(predicate);
					loj.add(obj);
				}
			}

			if(r.isResource()){
				++j;
				lr.add(r);
			}

			if (obj.isLiteral()) {
				++t;

				lo.add(obj); 
			}

			if(predicate.getLocalName().equals("type")) {
				lrtype.add(r.getURI()); //liste de resources eyant un type(avec redondance)
				lptype.add(obj.toString());
			}
		}
		//Nombre de sommet du model
		System.out.println("\n\t\t\t Nombre de Sommet(s) du graphe: " +dimModel);
		
		System.out.println("\n**********Statistiques**********");
		long p=lp.stream().distinct().count();//Nombre de propriété differente
		long lg =lr.stream().distinct().count();//Nombre de ressources différente
		long n = lo.stream().distinct().count();//Nombre de littéral différent
		
		System.out.println("Nombre Triples du graphe: "+i +"\nNombre de Resources du graphe: "+(dimModel-t) +"\nNombre de propriétées: "+i +"\nNombre de Litteral: "+t );
		System.out.println("Donc... "+"\nNombre de ressources differentes: "+lg +"\nNombre Proprietées différentes: "+p +"\nNombre de Litteral different: "+n+"\n");
		
		ArrayList<Property> lptmp=new ArrayList<Property>();
		ArrayList<Property> lpdistinct=new ArrayList<Property>();
		for (Property property : lp) {	
			if(!lptmp.contains(property)) {
				lpdistinct.add(property);
				System.out.println("La propriété " + "\"" +property.getLocalName() + "\"" + " Nombre Occurences: "+Collections.frequency(lp,property));	
			}
			if(lvoc.contains(property.getNameSpace())) {
				nbrePropOfVocab++;
			}
			lptmp.add(property);

		}
		System.out.println("****Nombre de proprieté provenant du vocabulaire de ce dataset:  " +nbrePropOfVocab);	
		System.out.println("****Nombre de proprieté provenant D'AUCUN vocabulaire de ce dataset:  " +(i-nbrePropOfVocab));
	
		/*
		 * Characteristiques sur les proprietées
		 */
		System.out.println("\n**********Statistique sur littéraux a valeur numérique**********");

		ArrayList<String>lol =new ArrayList<String>();
		ArrayList<Integer>loin =new ArrayList<Integer>();
		for (RDFNode l : loj) {
			lol.add(l.toString());

		}

		for (String s : lol) {
			if (isInt(s)) {
				loin.add(Integer.parseInt(s));
			}
		}

		if(!loin.isEmpty()) {
			System.out.println("Ecco les valeurs numeriques du model : \n"+loin);
		}
		OptionalDouble avg = loin.stream().mapToInt(Integer::intValue).average();
		try{
			System.out.println("Valeur manimale :"+Collections.max(loin)+"\nValeur manimale :"+Collections.min(loin)+"\nMoyenne :"+avg.getAsDouble());
		}catch(NoSuchElementException e){ 
			//	e.printStackTrace();
			System.out.println("Opps...Ce model ne contient pas de value numerique");
		}
		
		System.out.println("\n******Caraterisation ressources typées********");
		ArrayList<String> lrtmptype=new ArrayList<String>();
		ArrayList<String> lrdistinct=new ArrayList<String>(); //liste de ressources typées
		ArrayList<Integer> lrtypefreq=new ArrayList<Integer>();
		for (String resource : lrtype) {	
			if(!lrtmptype.contains(resource)) {
				lrdistinct.add(resource);
				int fq=Collections.frequency(lrtype,resource);
				System.out.println("Ressource:* <"+resource +"> possède: " +fq +" type(s).");	
				lrtypefreq.add(fq);
			}

			lrtmptype.add(resource);
		}
		
		System.out.println("\nNombre de resources typées:* "+lrdistinct.size() +"\nNombre de type distinct:* "+lptype.stream().distinct().count());
		try{
			System.out.println("**********Statistique sur nombre de type par resources**********" +"\nMin:* "+Collections.min(lrtypefreq) +"\nMax:* "+Collections.max(lrtypefreq) + "\nMoyenne:* "+lrtypefreq.stream().mapToInt(Integer::intValue).sum()/lrtypefreq.size() );
		}catch(NoSuchElementException e){
			//	e.printStackTrace();
			System.out.println("\nOpps...Impossible de faire des statistiques car ne contient pas de resources typées");
		}
		// print the graph as RDF/XML
		//model.write(System.out, "RDF/XML-ABBREV");
        // model.write(System.out, "N-TRIPLE");
		// model.write(System.out, "N3");
		System.out.println("**********Vocabulaire utilisé*********");
		System.out.println("Nombre de vocabulaire de ce dataset: " + st +"\nListe des vocabulaires:" + lvocAbrege);
		System.out.println("\n**********fin profillage**********\n");

	}

	public static boolean isInt(String str){
		return (str.lastIndexOf("-") == 0 && !str.equals("-0")) ? str.replace("-", "").matches(
				"[0-9]+") : str.matches("[0-9]+");
	}
	
	
}
